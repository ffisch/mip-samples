import mip

def main():
    m = mip.Model(sense=mip.MAXIMIZE, solver_name=mip.CBC)
    print(f'sense={m.sense}')
    print(f'solver={m.solver_name}')

    x = m.add_var(name='x')

    n = 10
    y = [m.add_var(name=f'a{i}', var_type=mip.BINARY) for i in range(n)]
    print([str(v) for v in y])
    z = m.add_var(name='z_cost', var_type=mip.INTEGER, lb=-10, ub=10)
    print(z, z.lb, z.ub)
    vz = m.var_by_name('z_cost')
    vz.ub = 5
    print(vz, vz.lb, vz.ub)

    m += (x + mip.xsum(y)) <= 10

if __name__ == '__main__':
    main()