import time

import mip


def main():
    profits = [10, 13, 18, 31, 7, 15]
    weights = [11, 15, 20, 35, 10, 33]
    capacity = 47
    item_indices = range(len(weights))

    m = mip.Model('knapsack')

    x = [m.add_var(var_type=mip.BINARY) for i in item_indices]

    m.objective = mip.maximize(mip.xsum([profits[i] * x[i] for i in item_indices]))

    m += mip.xsum(weights[i] * x[i] for i in item_indices) <= capacity

    m.verbose = 0
    status = m.optimize()
    print(status)

    selected = [i for i in item_indices if x[i].x >= 0.99]
    print(selected)


if __name__ == '__main__':
    main()
